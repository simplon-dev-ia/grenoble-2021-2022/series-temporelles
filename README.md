# Activité sur les séries temporelles

![series temporelles](https://cdn.pixabay.com/photo/2015/02/01/22/37/hourglass-620397_960_720.jpg)

## Recherche d'informations

- Trouver des exemples de cas d'usages de séries temporelles

- Définir les termes : tendance, saisonnalité, cycle, résidu, stationnaire, auto-regression, moyenne mobile

- Expliquer le fonctionnement des modèles suivants : lissage exponentiel, Holt, Holt-Winters, ARMA, ARIMA, SARIMA

## Ressources

- https://www.youtube.com/watch?v=4hrMdu9CSQs&list=PLvcbYUQ5t0UHOLnBzl46_Q6QKtFgfMGc3&index=21

- https://medium.com/dataman-in-ai/anomaly-detection-for-time-series-a87f8bc8d22e